# Mastodon Newsbot

Mastodon newsbot is a small python script that automatically posts toots to a Mastodon instance from a set of RSS/Atom feeds.

## Installation
Newsbot requires Python 3 (because SSL with Python 2 is so painful).

1. Install the Mastodon.py library with `pip3 install Mastodon.py`.

2. Copy `newsbot.py` and `newsbot.yaml` to a convenient directory.

3. Make `newsbot.py` executable (`chmod u+x newsbot.py` on linux).

4. Now you can register your application with your Mastodon instance. Let's say your instance is at `http://your_mastodon_instance.com`:

		python3
		>>> from mastodon import Mastodon
		>>> Mastodon.create_app('newsbot', api_base_url='http://your_mastodon_instance.com')
		('your_app_id', 'your_app_secret')
		>>>quit()

	Note down `your_app_id` and `your_app_secret`.

5. Sign up for a newsbot account at your Mastodon instance, verify it by clicking the emailed link, and log in.

6. Edit your profile, tick "This is a bot account", and save changes.

7. Edit the parameters in `newsbot.yaml`.

## Configuration
Newsbot gets it's settings from `newsbot.yaml`, which must be in the same directory as `newsbot.py`.

	app_debug: true
	app_id: your_app_id
	app_secret: your_app_secret
	app_url: http://your_mastodon_instance.com
	app_username: your_app_username@your_mastodon_instance.com
	app_userpass: your_app_userpass
	feeds:
	- name: Red News
	  url: http://rednews.com/feed/rss
	- name: Blue News
	  url: http://bluenews/rss/national.xml

- *app_debug*: When true, newsbot will get items from feeds and print them instead of tooting them. When everything is working, change this to false.
- *app_id*: `*your_app_id*` that you noted down when registering your application in step 3.
- *app_secret*: `*your_app_secret*` that you noted down when registering your application in step 3.
- *app_url*: The URL for your Mastodon instance (including the `https://`).
- *app_username*: The email address for the newsbot account you created in step 4.
- *app_userpass*: The password for the newsbot account you created in step 4.
- *feed name*: A short decriptive name for the feed
- *feed url*: The URL for the feed (including the `https://`).

For each feed you want newsbot to toot, add name and url lines to the feeds section of newsbot.yaml.

## Testing
Newsbot only toots items that have appeared since the last time it ran. It does this by storing a timestamp for each feed in `newsbot.yaml`.

The first time you run newsbot it won't print/toot anything, but it will add an "xlast" entry (the unix timestamp of the last posted item) for each feed like this:

	- name: Red News
	  url: http://rednews.com/feed/rss
	  xlast: 1535576400

From now on, newsbot will print/toot any items newer than `xlast` (and update `xlast` in `newsbot.yaml`). You should see something like this:

	--- DEBUG MODE ---
	In debug mode, newsbot.py will print feed items instead of tooting them.
	To switch off debug mode, edit newsbot.yaml and change app_debug from
	true to false.
	Getting items from Red News...
		1535589961 Trump fires himself
		1535585272 Kim Kardashian sneezes
	Getting items from Blue News...
		1535585415 New Zealand beats Spain in thrilling world cup final
	Processed 2 feeds in 1.52 seconds.

When everything works correctly, edit `newsbot.yaml` and change `app_debug` to `false`. Newsbot will now toot any new items to your Mastodon instance.

The final task is to make it run automatically. On linux, you can use cron to run every ten minutes:

	crontab -e
	*/10 * * * * /home/someone/bin/newsbot.py


