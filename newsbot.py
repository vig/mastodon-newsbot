#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
newsbot.py: Post toots to a Mastodon instance from a set of rss/atom feeds
defined in newsbot.yaml.

Copyright 2018 Greg Fawcett greg@vig.co.nz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Version 1.1
"""

import sys
import time
import yaml
import urllib3
from lxml import etree
from mastodon import Mastodon
from email.utils import parsedate_tz, mktime_tz

setup_instructions = '''

Newsbot gets it's settings from a configuration file called newsbot.yaml, which
must be in the same directory as newsbot.py. newsbot.yaml should look like this:

app_debug: true
app_id: your_app_id
app_secret: your_app_secret
app_url: your_app_url
app_username: your_app_username
app_userpass: your_app_userpass
feeds:
- name: Newsroom
  url: http://newsroom.co.nz/feed/rss
- name: Otago Daily Times
  url: http://odt.co.nz/news/feed
- name: RNZ
  url: https://www.radionz.co.nz/rss/national.xml

your_app_url: This is the URL of the Mastodon instance (e.g. https://mastodon.social).
your_app_id and your_app_secret: These register your app with the instance. You can
generate them by running python3 and entering the following commands:

>>> from mastodon import Mastodon
>>> Mastodon.create_app('newsbot', api_base_url=your_app_url)
('your_app_id', 'your_app_secret')

your_app_username and your_app_userpass: Sign up as a new user "NewsBot" on your
instance, and verify the email address you used for this. That email is
your_app_username and the password you created is your_app_userpass.

For each feed you want newsbot to toot, add name and url lines to the feeds section
of newsbot.yaml.

When you first run newsbot it won't toot anything, but it will
add an "xlast" entry for each feed like this:

- name: Newsroom
  url: http://newsroom.co.nz/feed/rss
  xlast: 1535576400

xlast is the unix timestamp of the last posted item for that feed. When you run
newsbot again, it will toot any new items, and update xlast in newsbot.yaml.
'''

#----------------------------------------------------------------------
# Remove tags from a string.
def detag(s):
	epos = s.find('>')
	while epos!=-1: # Found tag closer ...
		spos = s.rfind('<', 0, epos) # ...is there a preceding tag opener?
		if spos!=-1: # There is! Cut out this chunk.
			s = s[:spos]+s[epos+1:]
			epos = s.find('>', spos) # Search again, from where chunk started
		else: # No tag opener - remove orphan tag closer and search again
			s = s[:epos]+s[epos+1:]
			epos = s.find('>', epos+1) # Search again, from next character
	return s.replace('<', '') # Remove any orphan tag openers


#----------------------------------------------------------------------
# feed is a dict with name, url and xlast ('x' so it appears after name and url) items
def toot_new_news(feed, http, mastodon, debug):
	if debug:
		print('Getting items from %s...'%feed['name'])
	xlast = latest_itime = feed['xlast'] if 'xlast' in feed else int(time.time())
	try:
		req = http.request('GET', feed['url'], preload_content=False, release_conn=False)
		try:
			tree = etree.parse(req)
		except:
			import traceback
			print(f'Failed to parse feed from %s: '%feed['name'])
			print(traceback.format_exc())
			return
		for el in tree.getroot():
			if el.tag=='channel' or el.tag=='feed':
				for item in el.findall('item' if el.tag=='channel' else 'entry'):
					ititle = ilink = itime = None
					for item_child in item:
						tag = item_child.tag.lower()
						if tag=='title':
							ititle=detag(item_child.text.strip())
						elif tag=='link':
							ilink = detag(item_child.text.strip())
						elif tag=='pubdate' or tag=='published':
							itime = mktime_tz(parsedate_tz(detag(item_child.text.strip())))
					if ititle and ilink and itime and itime > xlast:
						if debug:
							print(f'\t{itime} {ititle}')
						else:
							mastodon.status_post(f'{ititle} {ilink}')
						if itime > latest_itime:
							latest_itime = itime
		feed['xlast'] = latest_itime
	finally:
		req.release_conn()


#----------------------------------------------------------------------
if __name__ == '__main__':

	# Load feed sites from newsbot.yaml
	try:
		yaml_filename = __file__.replace('.py', '.yaml')
		f = open(yaml_filename, 'r')
		config = yaml.load(f.read())
		assert 'app_debug' in config, 'app_debug'
		assert 'app_id' in config, 'app_id'
		assert 'app_secret' in config, 'app_secret'
		assert 'app_url' in config, 'app_url'
		assert 'app_username' in config, 'app_username'
		assert 'app_userpass' in config, 'app_userpass'
		assert 'feeds' in config, 'feeds'
	except FileNotFoundError:
		sys.exit(f'ERROR: Cannot open {yaml_filename}.'+setup_instructions)
	except AssertionError as err:
		sys.exit(f'ERROR: missing {err} setting in {yaml_filename}.'+setup_instructions)


	urllib3.disable_warnings() # To silence warnings about SNI missing.
	http = urllib3.PoolManager()
	mastodon = Mastodon(config['app_id'], config['app_secret'], api_base_url=config['app_url'])
	mastodon.log_in(config['app_username'], config['app_userpass'])
	if config['app_debug']:
		print('''--- DEBUG MODE ---
In debug mode, newsbot.py will print feed items instead of tooting them.
To switch off debug mode, edit newsbot.yaml and change app_debug from
true to false.''')
		start = time.time()

	# Toot any new new items from each feed
	for feed in config['feeds']:
		toot_new_news(feed, http, mastodon, config['app_debug'])

	# Save feed sites with update last time
	if config['app_debug']:
		print('Processed {} feeds in {:0.2f} seconds.'.format(len(config['feeds']), time.time()-start))
	with open(yaml_filename, 'w') as f:
		f.write(yaml.dump(config, default_flow_style=False))
